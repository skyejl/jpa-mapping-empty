package com.twuc.webApp;

import javax.persistence.*;

@Entity
public class Staff {
    @Id
    private Long id;
    @Embedded
    @AttributeOverrides({
            @AttributeOverride( name = "firstName", column = @Column(name = "firstName")),
            @AttributeOverride( name = "lastName", column = @Column(name = "lastName")),
    })
    private Name name;

    public Staff() {
    }

    public Staff(Long id, Name name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public Name getName() {
        return name;
    }
}
