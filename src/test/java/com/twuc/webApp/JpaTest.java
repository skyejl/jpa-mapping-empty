package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings("SpringTestingDirtiesContextInspection")
@DataJpaTest(showSql = false)
@DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
class JpaTest {
    @Autowired
    private OfficeRepository repository;
    @Autowired
    private StaffRepository staffRepository;

    @Autowired
    private EntityManager em;

    @Test
    void office_test() {
        Office office = repository.save(new Office(1L, "Xi An"));
        em.flush();
        assertNotNull(office);
    }

    @Test
    void should_get_by_em() {
        em.persist(new Office(1L, "Xi An"));
        em.flush();
        em.clear();
        Office office = em.find(Office.class, 1L);
        assertEquals(Long.valueOf(1L), office.getId());
        assertEquals("Xi An", office.getCity());
    }

    @Test
    void should_throw_exception_when_city_null() {

        assertThrows(PersistenceException.class, ()-> {
            Office office = repository.save(new Office(1L, null));
            em.flush();
        });
    }

    @Test
    void should_city_length_less_36() {
        assertThrows(PersistenceException.class, ()-> {
            Office office = repository.save(new Office(1L, "scscddddddscscddddddscscddddddscscdddddd"));
            em.flush();
        });
    }
    @Test
    void staff_test() {
        Staff staff = staffRepository.save(new Staff(1L, new Name("skye", "Davis")));
        em.flush();
        assertNotNull(staff);
    }
}
